<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @since 2023-01-15
 */

require_once("./GraphImporter.php");
require_once("./JsonToGraphSplitter.php");
require_once("./fetcher.php");

$graphImporter = new GraphImporter();
$jsonSplitter = new JsonToGraphSplitter($graphImporter);

foreach ($sources as $sourceIndex => $source)
{
    $jsonSplitter->jsonToGraph("./input/".$sourceIndex);
}

?>
