<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @attention This method reads all of the input into memory.
 * @author Stephan Kreutzer
 * @since 2023-01-11
 */


require_once("./GraphImporter.php");


class JsonToGraphSplitter
{
    function __construct($graphImporter)
    {
        $this->graphImporter = $graphImporter;
    }

    function jsonToGraph($inputPath)
    {
        $this->graphImporter->reset();

        $input = file_get_contents($inputPath);
        $input = json_decode($input);

        $nodeIdCurrent = 1;

        $this->graphImporter->insertNodeMerge($nodeIdCurrent, $input->url);
        $nodeIdCurrent++;

        $nodeIdCurrent = $this->handleLists($input->lists, $input->url, $nodeIdCurrent - 1, $nodeIdCurrent);

        $this->graphImporter->reset();
    }

    /** @todo Nothing done for passing $lists per reference! */
    function handleLists($lists, $sourceUrl, $nodeIdParent, $nodeIdCurrent)
    {
        for ($listIndex = 0, $listMax = count($lists);
             $listIndex < $listMax;
             $listIndex++)
        {
            if (isset($lists[$listIndex]->url) == true)
            {
                $this->graphImporter->insertNodeMerge($nodeIdCurrent, $lists[$listIndex]->url."/".$lists[$listIndex]->name);
            }
            else
            {
                $this->graphImporter->insertNodeMerge($nodeIdCurrent, $sourceUrl."/".$lists[$listIndex]->name);
            }

            $this->graphImporter->insertEdge($nodeIdCurrent, $nodeIdParent);

            $nodeIdCurrent++;

            $nodeIdCurrent = $this->handleBooks($lists[$listIndex]->books, $nodeIdCurrent - 1, $nodeIdCurrent);
        }

        return $nodeIdCurrent;
    }

    /** @todo Nothing done for passing $books per reference! */
    function handleBooks($books, $nodeIdParent, $nodeIdCurrent)
    {
        for ($bookIndex = 0, $bookMax = count($books);
             $bookIndex < $bookMax;
             $bookIndex++)
        {
            $this->graphImporter->insertNodeMerge($nodeIdCurrent, $books[$bookIndex]->title);
            $this->graphImporter->insertEdge($nodeIdCurrent, $nodeIdParent);

            $nodeIdCurrent++;
        }

        return $nodeIdCurrent;
    }

    protected $graphImporter = null;
}

?>
