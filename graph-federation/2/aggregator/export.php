<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @since 2023-01-15
 */


if (isset($_GET["format"]) !== true)
{
    exit(0);
}

if ($_GET["format"] != "json")
{
    exit(0);
}


$mysqlConnection = mysqli_connect("localhost", "root", "");

if ($mysqlConnection != false)
{
    if (mysqli_query($mysqlConnection, "USE graph") == false)
    {
        mysqli_close($mysqlConnection);
        throw new Exception("Database not found.");
    }
}
else
{
    throw new Exception("Can't connect to database.");
}


$sourceNodes = mysqli_query($mysqlConnection,
                            "SELECT `node`.`id`,\n".
                            "    `node`.`value`\n".
                            "FROM `node`\n".
                            "LEFT JOIN `edge`\n".
                            "ON `node`.`id`=`edge`.`source`\n".
                            "WHERE `edge`.`source` IS NULL\n");

if ($sourceNodes === false)
{
    throw new Exception("Database query error.");
}

$sourceNodes = getDbResultAssoc($sourceNodes);
$sourcePerId = array();

foreach ($sourceNodes as $sourceNode)
{
    if (array_key_exists((int)$sourceNode["id"], $sourcePerId) == true)
    {
        throw new Exception("Duplicate in source nodes.");
    }

    $sourcePerId[(int)$sourceNode["id"]] = $sourceNode["value"];
}

$query = "";

for ($i = 0, $max = count($sourceNodes); $i < $max; $i++)
{
    if ($i > 0)
    {
        $query .= " OR `target`=";
    }

    $query .= $sourceNodes[$i]["id"];
}

$listEdges = mysqli_query($mysqlConnection,
                          "SELECT `source`,\n".
                          "    `target`\n".
                          "FROM `edge`\n".
                          "WHERE `target`=".$query."\n");

if ($listEdges === false)
{
    throw new Exception("Database query error.");
}

$listEdges = getDbResultAssoc($listEdges);
$listsPerSource = array();

foreach ($listEdges as $listEdge)
{
    if (array_key_exists((int)$listEdge["source"], $listsPerSource) == true)
    {
        throw new Exception("Duplicate in list edges.");
    }

    $listsPerSource[(int)$listEdge["source"]] = (int)$listEdge["target"];
}

$query = "";

for ($i = 0, $max = count($listEdges); $i < $max; $i++)
{
    if ($i > 0)
    {
        $query .= " OR `edge`.`target`=";
    }

    $query .= $listEdges[$i]["source"];
}

$books = mysqli_query($mysqlConnection,
                      "SELECT `node`.`id` AS `node_id`,\n".
                      "    `node`.`value` AS `node_value`,\n".
                      "    `edge`.`target` AS `edge_target`\n".
                      "FROM `node`\n".
                      "LEFT JOIN `edge`\n".
                      "ON `node`.`id`=`edge`.`source`\n".
                      "WHERE `edge`.`target`=".$query."\n");

if ($books === false)
{
    throw new Exception("Database query error.");
}

$books = getDbResultAssoc($books);
$sourcesPerBook = array();

foreach ($books as $book)
{
    if (array_key_exists((int)$book["edge_target"], $listsPerSource) != true)
    {
        throw new Exception("List ID of book is missing in the lists-per-source mapping.");
    }

    $sourceNodeId = $listsPerSource[(int)$book["edge_target"]];

    if (array_key_exists($sourceNodeId, $sourcePerId) != true)
    {
        throw new Exception("Node ID as resolved from lists-per-source mapping doesn't exist.");
    }

    $bookSource = $sourcePerId[$sourceNodeId];

    if (array_key_exists($book["node_value"], $sourcesPerBook) != true)
    {
        $sourcesPerBook[$book["node_value"]] = array();
    }

    $sourcesPerBook[$book["node_value"]][] = $bookSource;
}


header("Content-Type: application/json");

echo "{\"books\":[";

$first = true;

foreach ($sourcesPerBook as $book => $sources)
{
    if ($first == true)
    {
        $first = false;
    }
    else
    {
        echo ",";
    }

    echo "{\"title\":".json_encode($book).",".
         "\"sources\":[";

    for ($i = 0, $max = count($sources); $i < $max; $i++)
    {
        if ($i > 0)
        {
            echo ",";
        }

        echo json_encode($sources[$i]);
    }

    echo "]}";
}

echo "]}";



function getDbResultAssoc($resultHandle)
{
    $result = array();

    while (true)
    {
        $temp = mysqli_fetch_assoc($resultHandle);

        if ($temp !== null)
        {
            $result[] = $temp;
        }
        else
        {
            break;
        }
    }

    mysqli_free_result($resultHandle);

    return $result;
}




?>
