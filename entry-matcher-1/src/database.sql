-- Copyright (C) 2023 Stephan Kreutzer
--
-- entry-matcher-1 is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License version 3 or any later version,
-- as published by the Free Software Foundation.
--
-- entry-matcher-1 is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License 3 for more details.
--
-- You should have received a copy of the GNU Affero General Public License 3
-- along with entry-matcher-1. If not, see <http://www.gnu.org/licenses/>.


START TRANSACTION;
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `matcher` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `matcher`;

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `employee_city` (
  `id_employee` int(11) NOT NULL,
  `id_city` int(11) NOT NULL
) DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


INSERT INTO `city` (`id`, `name`) VALUES
(1, 'Vienna'),
(2, 'Munich'),
(3, 'New York');

INSERT INTO `employee` (`id`, `name`) VALUES
(1, 'Martin'),
(2, 'Sarah'),
(3, 'Klaus'),
(4, 'Sandra');

INSERT INTO `employee_city` (`id_employee`, `id_city`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 2);

COMMIT;
