/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of entry-matcher-1.
 *
 * entry-matcher-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * entry-matcher-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with entry-matcher-1. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let basePath = "";

{
    let scripts = document.getElementsByTagName("script");
    // 'scripts' contains always the last script tag that was loaded.
    let myPath = scripts[scripts.length-1].src;

    basePath = myPath.substring(0, myPath.lastIndexOf('/'));
}

// 'file://' is bad.
if (basePath.substring(0, 7) == "file://")
{
    basePath = basePath.substr(8);
    basePath = "https://" + basePath;
}

function getConnection()
{
    let xmlhttp = null;

    // Mozilla
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    // IE
    else if (window.ActiveXObject)
    {
        xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
    }

    if (xmlhttp == null)
    {
        console.log("No AJAX.");
        return null;
    }

    return xmlhttp;
}

let cityList = null;
let employeeList = null;
let employeeCityMapping = null;

function init()
{
    cityList = new CityList();
    cityList.requestCities();

    employeeList = new EmployeeList();
    employeeList.requestEmployees();

    employeeCityMapping = new EmployeeCityMapping(employeeList, cityList);
    employeeCityMapping.requestMapping();
}

function CityList()
{
    let that = this;

    that.cities = new Map();

    that.requestCities = function()
    {
        that.cities = new Map();

        let xmlhttp = getConnection();

        xmlhttp.open("GET", basePath + "/api/city.php", false);
        xmlhttp.setRequestHeader("Accept", "application/json");
        xmlhttp.onreadystatechange = function() { _requestCitiesResponse(xmlhttp); };
        xmlhttp.send();

    };

    function _requestCitiesResponse(xmlhttp)
    {
        if (xmlhttp.readyState != 4)
        {
            // Waiting...
            return 0;
        }

        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            let result = JSON.parse(xmlhttp.responseText);

            for (let i = 0, max = result.length; i < max; i++)
            {
                that.cities.set(result[i].id, result[i].name);
            }
        }

        _renderCities();
    }

    function _renderCities()
    {
        let target = document.getElementById("mapping");

        if (target == null)
        {
            console.log("Element with ID 'mapping' not found.");
            return -1;
        }

        while (target.hasChildNodes() == true)
        {
            target.removeChild(target.lastChild);
        }

        for (const [key, value] of that.cities)
        {
            let container = document.createElement("div");
            container.setAttribute("id", "city-" + key);
            container.classList.add("city");
            container.setAttribute("ondrop", "drop(event);");
            container.setAttribute("ondragover", "allowDrop(event);");

            let span = document.createElement("span");
            span.classList.add("city-title");
            span.appendChild(document.createTextNode(value));
            container.appendChild(span);

            target.appendChild(container);
        }
    }
}

function EmployeeList()
{
    let that = this;

    that.employees = new Map();

    that.requestEmployees = function()
    {
        that.employees = new Map();

        let xmlhttp = getConnection();

        xmlhttp.open("GET", basePath + "/api/employee.php", true);
        xmlhttp.setRequestHeader("Accept", "application/json");
        xmlhttp.onreadystatechange = function() { _requestEmployeesResponse(xmlhttp); };
        xmlhttp.send();

    };

    function _requestEmployeesResponse(xmlhttp)
    {
        if (xmlhttp.readyState != 4)
        {
            // Waiting...
            return 0;
        }

        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            let result = JSON.parse(xmlhttp.responseText);

            for (let i = 0, max = result.length; i < max; i++)
            {
                that.employees.set(result[i].id, result[i].name);
            }
        }
    }
}

function EmployeeCityMapping(employeeList, cityList)
{
    let that = this;

    that.employeeList = employeeList;
    that.cityList = cityList;
    that.mapping = new Map();

    that.requestMapping = function()
    {
        that.mapping = new Map();

        let xmlhttp = getConnection();

        xmlhttp.open("GET", basePath + "/api/employee_city.php", true);
        xmlhttp.setRequestHeader("Accept", "application/json");
        xmlhttp.onreadystatechange = function() { _requestMappingResponse(xmlhttp); };
        xmlhttp.send();

    };

    function _requestMappingResponse(xmlhttp)
    {
        if (xmlhttp.readyState != 4)
        {
            // Waiting...
            return 0;
        }

        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            let result = JSON.parse(xmlhttp.responseText);

            for (let i = 0, max = result.length; i < max; i++)
            {
                that.mapping.set(result[i].id_employee, result[i].id_city);
            }
        }

        _renderMapping();
    }

    function _renderMapping()
    {
        for (const [key, value] of that.mapping)
        {
            let target = document.getElementById("city-" + value);

            if (target == null)
            {
                console.log("Element with ID 'city-" + value + "' not found.");
                return -1;
            }

            let container = document.createElement("div");
            container.setAttribute("id", "employee-" + key);
            container.classList.add("employee");
            container.setAttribute("draggable", "true");
            container.setAttribute("ondragstart", "drag(event);");
            container.appendChild(document.createTextNode(that.employeeList.employees.get(key)));

            if (that.employeeList.employees.get(key) == undefined)
            {
                alert("async!");
            }

            target.appendChild(container);
        }
    }

    that.save = function()
    {
        let target = document.getElementById("mapping");

        if (target == null)
        {
            console.log("Element with ID 'mapping' not found.");
            return -1;
        }

        let mapping = new Map();

        for (let i = 0, max = target.children.length; i < max; i++)
        {
            let node = target.children[i];

            if (node.nodeType != Node.ELEMENT_NODE)
            {
                continue;
            }

            if (node.classList.contains("city") != true)
            {
                continue;
            }

            // Not great... :-(
            let cityId = parseInt(node.getAttribute("id").split("-")[1]);

            for (let j = 0, max2 = node.children.length; j < max2; j++)
            {
                let node2 = node.children[j];

                if (node2.nodeType != Node.ELEMENT_NODE)
                {
                    continue;
                }

                if (node2.classList.contains("employee") != true)
                {
                    continue;
                }

                // Not great... :-(
                let employeeId = parseInt(node2.getAttribute("id").split("-")[1]);

                if (mapping.has(cityId) != true)
                {
                    mapping.set(cityId, new Array());
                }

                mapping.get(cityId).push(employeeId);
            }
        }

        let payload = "{\"cities\":[";
 
        let first = true;

        for (const [cityId, employees] of mapping)
        {
            if (first == true)
            {
                first = false;
            }
            else
            {
                payload += ",";
            }

            payload += "{\"id\":" + cityId + ",\"employees\":[";

            for (let i = 0, max = employees.length; i < max; i++)
            {
                if (i > 0)
                {
                    payload += ",";
                }

                payload += "{\"id\":" + employees[i] + "}";
            }

            payload += "]}";
        }

        payload += "]}";

        let xmlhttp = getConnection();

        xmlhttp.open("PUT", basePath + "/api/employee_city.php", false);
        xmlhttp.setRequestHeader("Content-Type", "application/json");
        //xmlhttp.onreadystatechange = function() { TODO }
        xmlhttp.send(payload);
    }
}

function drag(event)
{
    event.dataTransfer.setData("id", event.target.id);
}

function allowDrop(event)
{
    event.preventDefault();
}

function drop(event)
{
    event.preventDefault();

    let id = event.dataTransfer.getData("id");
    let target = event.target;

    do
    {
        if (target.classList.contains("city") == true)
        {
            break;
        }
        else
        {
            target = target.parentNode;
        }

    } while (target != null);

    if (target == null)
    {
        console.log("No drop area found.");
        return;
    }

    target.appendChild(document.getElementById(id));
}
