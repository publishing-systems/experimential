<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of entry-matcher-1.
 *
 * entry-matcher-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * entry-matcher-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with entry-matcher-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/api/employee.php
 * @author Stephan Kreutzer
 * @since 2023-01-26
 */


require_once("../libraries/database.inc.php");

if ($_SERVER['REQUEST_METHOD'] === "GET")
{
    $employees = Database::Get()->query("SELECT `id`,\n".
                                        "    `name`\n".
                                        "FROM `".Database::GetPrefix()."employee`\n".
                                        "WHERE 1\n");

    if ($employees == false)
    {
        http_response_code(500);
        exit(1);
    }

    $employees = Database::GetResultAssoc($employees);

    http_response_code(200);
    header("Content-Type: application/json");

    echo "[";

    for ($i = 0, $max = count($employees); $i < $max; $i++)
    {
        if ($i > 0)
        {
            echo ",";
        }

        echo "{\"id\":".((int)$employees[$i]['id']).",".
             "\"name\":".json_encode($employees[$i]['name'])."}";
    }

    echo "]";

    exit(0);
}
else
{
    http_response_code(405);
    exit(0);
}


?>
