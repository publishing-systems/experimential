<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of entry-manager-1.
 *
 * entry-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * entry-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with entry-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @since 2023-01-24
 */


require_once("./libraries/https.inc.php");

if (empty($_SESSION) === true)
{
    @session_start();
}

if (isset($_GET['logout']) === true &&
    isset($_SESSION['user_id']) === true)
{
    $_SESSION = array();

    if (isset($_COOKIE[session_name()]) == true)
    {
        setcookie(session_name(), '', time()-42000, '/');
    }
}

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>Entry Manager</title>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "  </head>\n".
     "  <body>\n";

if (isset($_GET['name']) !== true ||
    isset($_GET['password']) !== true)
{
    echo "    <div>\n".
         "      <h1>Entry Manager</h1>\n".
         "      <div>\n";

    if (isset($_SESSION['user_id']) === true &&
        isset($_SESSION['instance_path']) === true)
    {
        $lhs = str_replace("\\", "/", dirname(__FILE__));

        if ($lhs === $_SESSION['instance_path'])
        {
            require_once("./libraries/user_defines.inc.php");

            echo "        <ul>\n";

            if (((int)$_SESSION['user_role']) === USER_ROLE_ADMIN)
            {
                echo "          <li>\n".
                     "            <a href=\"entry_manage.php\">Manage entries</a>\n".
                     "          </li>\n";
            }
            else
            {
                echo "          <li>\n".
                     "            <a href=\"entry_manage.php\">Manage entries</a>\n".
                     "          </li>\n";
            }

            echo "        </ul>\n";
        }
        else
        {
            // This is a protection against a user gaining access to other
            // instances/installations of this software as hosted under the
            // same domain, as the cookie is valid for the entire domain.
            // To access a different instance, log out from the instance the
            // cookie/session is valid for, and then log in at the desired instance.
            echo "        <p>\n".
                 "          No access to this instance!\n".
                 "        </p>\n";
        }

        echo "        <form action=\"index.php?logout=true\" method=\"get\">\n".
             "          <fieldset>\n".
             "            <input type=\"submit\" name=\"logout\" value=\"Logout\"/><br/>\n".
             "          </fieldset>\n".
             "        </form>\n";
    }
    else
    {
        echo "        <p>\n".
             "          Welcome!\n".
             "        </p>\n".
             /** @attention Password here in the clear and too as GET parameter,
               * per the assumption there's no sensitive nor important information
               * here, and too so users can bookmark the login URL. */
             "        <form action=\"index.php\" method=\"get\">\n".
             "          <fieldset>\n".
             "            <input id=\"user_name\" name=\"name\" type=\"text\"/>\n".
             "            <label for=\"user_name\">User name</label><br/>\n".
             "            <input id=\"user_password\" name=\"password\" type=\"password\"/>\n".
             "            <label for=\"user_password\">Password</label><br/>\n".
             "            <input type=\"submit\" value=\"Login\"/>\n".
             "          </fieldset>\n".
             "        </form>\n";
    }

    echo "      </div>\n".
         "    </div>\n".
         "  </body>\n".
         "</html>\n";
}
else
{
    require_once("./libraries/user_management.inc.php");

    $user = NULL;

    $result = getUserByName($_GET['name']);

    if (is_array($result) !== true)
    {
        echo "    <div>\n".
             "      <p>\n".
             "        Error: Database problem.\n".
             "      </p>\n".
             "    </div>\n".
             "  </body>\n".
             "</html>\n";

        exit(1);
    }

    if (count($result) === 0)
    {
        echo "    <div>\n".
             "      <p>\n".
             "        Error: Login credentials invalid.\n".
             "      </p>\n".
             "      <a href=\"index.php\">Retry</a>\n".
             "    </div>\n".
             "  </body>\n".
             "</html>\n";

        exit(0);
    }
    else
    {
        // The user exists and wants to login.

        /** @attention "Password" currently saved as plain-text!
          * This is per the assumption, no sensitive nor important information
          * here, easier to add/maintain (and hand out credentials) users. */
        if ($result[0]['password'] === $_GET['password'])
        {
            $user = array("id" => (int)$result[0]['id'],
                          "role" => (int)$result[0]['role']);
        }
        else
        {
            echo "    <div>\n".
                 "      <p>\n".
                 "        Error: Login credentials invalid.\n".
                 "      </p>\n".
                 "      <a href=\"index.php\">Retry</a>\n".
                 "    </div>\n".
                 "  </body>\n".
                 "</html>\n";

            exit(0);
        }
    }

    if (is_array($user) === true)
    {
        $_SESSION = array();

        $_SESSION['instance_path'] = str_replace("\\", "/", dirname(__FILE__));
        $_SESSION['user_id'] = $user['id'];
        $_SESSION['user_name'] = $_GET['name'];
        $_SESSION['user_role'] = $user['role'];

        echo "    <div>\n".
             "      <p>\n".
             "        Login was successful.\n".
             "      </p>\n".
             "      <a href=\"index.php\">Continue</a>\n".
             "    </div>\n";
    }

    echo "  </body>\n".
         "</html>\n";
}


?>
