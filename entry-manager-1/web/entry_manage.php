<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of entry-manager-1.
 *
 * entry-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * entry-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with entry-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @since 2023-01-24
 */


require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");
require_once("./libraries/entry_management.inc.php");


$entries = getEntryByUserId((int)$_SESSION['user_id']);

if (is_array($entries) != true)
{
    http_response_code(500);
    exit(1);
}

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>Manage Entries</title>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "    <script type=\"text/javascript\" src=\"entry_manage.js\"></script>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div>\n".
     "      <table>\n".
     "        <thead>\n".
     "          <tr>\n".
     "            <th>Entry</th>\n".
     "            <th>Action</th>\n".
     "          </tr>\n".
     "        </thead>\n".
     "        <tbody id=\"table-entries\">\n";

for ($i = 0, $max = count($entries); $i < $max; $i++)
{
    $entry = $entries[$i];
    $entryId = (int)$entry['id'];

    echo "          <tr id=\"row-".$entryId."\">\n".
         "            <td id=\"value-".$entryId."\">".htmlspecialchars($entry['name'], ENT_XHTML, "UTF-8")."</td>\n".
         "            <td id=\"buttons-".$entryId."\"><button id=\"update-".$entryId."\" onclick=\"showEditControl(".$entryId.");\">edit</button> <button id=\"delete-".$entryId."\" onclick=\"deleteEntry(".$entryId.");\">delete</button></td>\n".
         "          </tr>\n";
}

echo "        </tbody>\n".
     "        <tfoot>\n".
     "          <tr>\n".
     "            <td><input id=\"new-entry\" type=\"text\" minlength=\"1\" required=\"required\"/></td>\n".
     "            <td><button onclick=\"addEntry();\">add</button></td>\n".
     "          </tr>\n".
     "        </tfoot>\n".
     "      </table>\n".
     "      <div>\n".
     "        <a href=\"index.php\">leave</a>\n".
     "      </div>\n";

echo "    </div>\n".
     "  </body>\n".
     "</html>\n";


?>
