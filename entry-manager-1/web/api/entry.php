<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of entry-manager-1.
 *
 * entry-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * entry-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with entry-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/api/entry.php
 * @author Stephan Kreutzer
 * @since 2023-01-25
 */


require_once("../libraries/https.inc.php");
require_once("../libraries/session.inc.php");
require_once("../libraries/entry_management.inc.php");

if ($_SERVER['REQUEST_METHOD'] === "POST")
{
    $payload = "";

    {
        $source = @fopen("php://input", "r");

        while (true)
        {
            $chunk = @fread($source, 1024);

            if ($chunk == false)
            {
                break;
            }

            $payload .= $chunk;
        }
    }

    $payload = json_decode($payload, true);

    if ($payload === false)
    {
        http_response_code(400);
        exit(0);
    }

    if (is_array($payload) != true)
    {
        http_response_code(400);
        exit(0);
    }

    if (isset($payload['name']) != true)
    {
        http_response_code(400);
        echo "'name' is missing.";
        exit(0);
    }

    $id = insertEntry($payload['name']);

    if ($id <= 0)
    {
        http_response_code(500);
        exit(1);
    }

    http_response_code(201);
    header("Content-Type: application/json");

    echo "{\"id\":".$id."}";

    exit(0);
}
else if ($_SERVER['REQUEST_METHOD'] === "PUT")
{
    if (isset($_GET['id']) != true)
    {
        http_response_code(400);
        echo "'id' is missing.";
        exit(0);
    }

    $id = (int)$_GET['id'];
    $payload = "";

    {
        $source = @fopen("php://input", "r");

        while (true)
        {
            $chunk = @fread($source, 1024);

            if ($chunk == false)
            {
                break;
            }

            $payload .= $chunk;
        }
    }

    $payload = json_decode($payload, true);

    if ($payload === false)
    {
        http_response_code(400);
        exit(0);
    }

    if (is_array($payload) != true)
    {
        http_response_code(400);
        exit(0);
    }

    if (isset($payload['name']) != true)
    {
        http_response_code(400);
        echo "'name' is missing.";
        exit(0);
    }

    $result = updateEntry($id, $payload['name']);

    if ($result !== true)
    {
        http_response_code(500);
        exit(1);
    }

    http_response_code(200);

    exit(0);
}
else if ($_SERVER['REQUEST_METHOD'] === "DELETE")
{
    if (isset($_GET['id']) != true)
    {
        http_response_code(400);
        echo "'id' is missing.";
        exit(0);
    }

    $id = (int)$_GET['id'];

    $result = deleteEntry($id);

    if ($result !== true)
    {
        http_response_code(500);
        exit(1);
    }

    http_response_code(204);

    exit(0);
}
else
{
    http_response_code(405);
    exit(0);
}



?>
