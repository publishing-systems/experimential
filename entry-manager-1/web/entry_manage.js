/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of entry-manager-1.
 *
 * entry-manager-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * entry-manager-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with entry-manager-1. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let xmlhttp = null;

// Mozilla
if (window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
// IE
else if (window.ActiveXObject)
{
    xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
}

if (xmlhttp == null)
{
    console.log("No AJAX.");
}

let basePath = "";

{
    let scripts = document.getElementsByTagName("script");
    // 'scripts' contains always the last script tag that was loaded.
    let myPath = scripts[scripts.length-1].src;

    basePath = myPath.substring(0, myPath.lastIndexOf('/'));
}

// 'file://' is bad.
if (basePath.substring(0, 7) == "file://")
{
    basePath = basePath.substr(8);
    basePath = "https://" + basePath;
}

function showEditControl(id)
{
    let target = document.getElementById("value-" + id);

    if (target == null)
    {
        console.log("Element with ID 'value-" + id + "' not found.");
        return -1;
    }

    let value = target.innerText;
    target.innerText = "";

    let input = document.createElement("input");
    input.setAttribute("id", "edit-" + id);
    input.setAttribute("type", "text");
    input.setAttribute("value", value);

    target.appendChild(input);

    target = document.getElementById("update-" + id);

    if (target == null)
    {
        console.log("Element with ID 'update-" + id + "' not found.");
        return -1;
    }

    target.innerText = "cancel";
    target.setAttribute("onclick", "hideEditControl(" + id + ", false);");

    target = document.getElementById("delete-" + id);

    if (target == null)
    {
        console.log("Element with ID 'delete-" + id + "' not found.");
        return -1;
    }

    target.parentNode.removeChild(target);

    target = document.getElementById("buttons-" + id);

    if (target == null)
    {
        console.log("Element with ID 'buttons-" + id + "' not found.");
        return -1;
    }

    target.insertBefore(document.createTextNode(" "), target.firstChild);

    let button = document.createElement("button");
    button.setAttribute("id", "confirm-" + id);
    button.setAttribute("onclick", "updateEntry(" + id + ");");
    button.appendChild(document.createTextNode("save"));
    target.insertBefore(button, target.firstChild);

    return 0;
}

function hideEditControl(id, updated)
{
    let target = document.getElementById("edit-" + id);

    if (target == null)
    {
        console.log("Element with ID 'edit-" + id + "' not found.");
        return -1;
    }

    let value = null;

    if (updated == true)
    {
        value = target.value;
    }
    else
    {
        value = target.getAttribute("value");
    }

    target.parentNode.removeChild(target);

    target = document.getElementById("value-" + id);

    if (target == null)
    {
        console.log("Element with ID 'value-" + id + "' not found.");
        return -1;
    }

    target.appendChild(document.createTextNode(value));

    target = document.getElementById("update-" + id);

    if (target == null)
    {
        console.log("Element with ID 'update-" + id + "' not found.");
        return -1;
    }

    target.innerText = "edit";
    target.setAttribute("onclick", "showEditControl(" + id + ");");

    target = document.getElementById("confirm-" + id);

    if (target == null)
    {
        console.log("Element with ID 'confirm-" + id + "' not found.");
        return -1;
    }

    target.parentNode.removeChild(target);

    target = document.getElementById("buttons-" + id);

    if (target == null)
    {
        console.log("Element with ID 'buttons-" + id + "' not found.");
        return -1;
    }

    let button = document.createElement("button");
    button.setAttribute("id", "delete-" + id);
    button.setAttribute("onclick", "deleteEntry(" + id + ");");
    button.appendChild(document.createTextNode("delete"));
    target.appendChild(button);
}

function addEntry()
{
    let source = document.getElementById("new-entry");

    if (source == null)
    {
        console.log("Element with ID 'new-entry' not found.");
        return -1;
    }

    if (source.value.length <= 0)
    {
        return 0;
    }

    xmlhttp.open("POST", basePath + "/api/entry.php", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.onreadystatechange = addEntryResult;
    xmlhttp.send("{\"name\":" + JSON.stringify(source.value) + "}");

    return 0;
}

function addEntryResult()
{
    if (xmlhttp.readyState != 4)
    {
        // Waiting...
        return 0;
    }

    if (xmlhttp.readyState == 4 && xmlhttp.status == 201)
    {
        let result = JSON.parse(xmlhttp.responseText);
        let entryNumber = result.id;

        let source = document.getElementById("new-entry");

        if (source == null)
        {
            console.log("Element with ID 'new-entry' not found.");
            return -1;
        }

        let value = source.value;
        source.value = "";

        let target = document.getElementById("table-entries");

        if (target == null)
        {
            console.log("Element with ID 'table-entries' not found.");
            return -1;
        }

        let row = document.createElement("tr");
        row.setAttribute("id", "row-" + entryNumber);

        let cellValue = document.createElement("td");
        cellValue.setAttribute("id", "value-" + entryNumber);
        cellValue.appendChild(document.createTextNode(value));
        row.appendChild(cellValue);

        let cellAction = document.createElement("td");
        cellAction.setAttribute("id", "buttons-" + entryNumber);

        let buttonEdit = document.createElement("button");
        buttonEdit.setAttribute("id", "update-" + entryNumber);
        buttonEdit.setAttribute("onclick", "showEditControl(" + entryNumber + ");");
        buttonEdit.appendChild(document.createTextNode("edit"));
        cellAction.appendChild(buttonEdit);

        cellAction.appendChild(document.createTextNode(" "));

        let buttonDelete = document.createElement("button");
        buttonDelete.setAttribute("id", "delete-" + entryNumber);
        buttonDelete.setAttribute("onclick", "deleteEntry(" + entryNumber + ");");
        buttonDelete.appendChild(document.createTextNode("delete"));
        cellAction.appendChild(buttonDelete);

        row.appendChild(cellAction);
        target.appendChild(row);

        source.focus();
    }
}

function updateEntry(id)
{
    let source = document.getElementById("edit-" + id);

    if (source == null)
    {
        console.log("Element with ID 'edit-" + id + "' not found.");
        return -1;
    }

    if (source.value == source.getAttribute("value"))
    {
        hideEditControl(id, false);

        return 0;
    }

    xmlhttp.open("PUT", basePath + "/api/entry.php?id=" + id, true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.onreadystatechange = function() { updateEntryResult(id); }
    xmlhttp.send("{\"name\":" + JSON.stringify(source.value) + "}");

    return 0;
}

function updateEntryResult(id)
{
    if (xmlhttp.readyState != 4)
    {
        // Waiting...
        return 0;
    }

    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
    {
        hideEditControl(id, true);

        return 0;
    }

    return 0;
}

function deleteEntry(id)
{
    xmlhttp.open("DELETE", basePath + "/api/entry.php?id=" + id, true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.onreadystatechange = function() { deleteEntryResult(id); }
    xmlhttp.send();

    return 0;
}

function deleteEntryResult(id)
{
    if (xmlhttp.readyState != 4)
    {
        // Waiting...
        return 0;
    }

    if (xmlhttp.readyState == 4 && xmlhttp.status == 204)
    {
        let target = document.getElementById("row-" + id);

        if (target == null)
        {
            console.log("Element with ID 'row-" + id + "' not found.");
            return -1;
        }

        target.parentNode.removeChild(target);

        return 0;
    }

    return 0;
}
