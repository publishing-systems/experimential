/* Copyright (C) 2018-2020 Stephan Kreutzer
 *
 * This file is part of change_tracking_text_editor_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * change_tracking_text_editor_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * change_tracking_text_editor_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with change_tracking_text_editor_1. If not, see <http://www.gnu.org/licenses/>.
 */



#include "TrackingTextEdit.h"



const int EDITMODE_NONE = 0;
const int EDITMODE_ADD = 1;
const int EDITMODE_DELETE = 2;



TrackingTextEdit::TrackingTextEdit():
  m_bKeyPressEvent(false),
  m_nEditMode(EDITMODE_NONE),
  //m_nAutosaveCharacters(-1), // TODO
  m_nAutosaveCharacters(20),
  m_nPositionLast(textCursor().position()),
  m_nPositionOperationStart(-1),
  m_nTextLengthLast(toPlainText().length())
{
    QFont aMonospaceFont("TypeWriter");
    aMonospaceFont.setStyleHint(QFont::TypeWriter);
    setFont(aMonospaceFont);

    // TODO: Prevent caller from changing this.
    setUndoRedoEnabled(false);
    setOverwriteMode(false);
    setTextInteractionFlags(Qt::TextEditable);

    connect(this, SIGNAL(cursorPositionChanged()),
            this, SLOT(cursorPositionChanged()));

    m_aOutputWriter.open("output.xml", std::ofstream::out | std::ofstream::app);

    if (m_aOutputWriter.is_open() == true &&
        m_aOutputWriter.good() == true)
    {
        m_aOutputWriter << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                        << "<!-- This file was created by change_tracking_text_editor_1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and https://publishing-systems.org). -->\n"
                        << "<change-tracking-text-editor-1-text-history xmlns=\"htx-scheme-id://org.hypertext-systems.20180702T071630Z/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1.20181103T000000Z\">\n";
        m_aOutputWriter.flush();
    }
    else
    {
        // TODO
    }
}

void TrackingTextEdit::keyPressEvent(QKeyEvent* pEvent)
{
    m_bKeyPressEvent = true;

    QPlainTextEdit::keyPressEvent(pEvent);

    int nPositionCurrent(textCursor().position());
    int nTextLengthCurrent(toPlainText().length());

    if (pEvent->text().isEmpty() != true)
    {
        if (pEvent->key() == Qt::Key_Backspace)
        {
            if (m_nTextLengthLast > nTextLengthCurrent)
            {
                if (m_nEditMode == EDITMODE_NONE)
                {
                    m_nPositionOperationStart = m_nPositionLast;
                }
                else if (m_nEditMode == EDITMODE_DELETE)
                {
                    // Ignore.
                }
                else if (m_nEditMode == EDITMODE_ADD)
                {
                    if (m_aOutputWriter.is_open() == true &&
                        m_aOutputWriter.good() == true)
                    {
                        m_aOutputWriter << "<add position=\"" << m_nPositionOperationStart << "\">";

                        QString strCharacters(toPlainText().mid(m_nPositionOperationStart, nPositionCurrent - m_nPositionOperationStart));

                        if (m_strLastCharacterAdded.isEmpty() != true)
                        {
                            strCharacters += m_strLastCharacterAdded;
                        }

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        strCharacters.replace("&", "&amp;");
                        strCharacters.replace("<", "&lt;");
                        strCharacters.replace(">", "&gt;");

                        m_aOutputWriter << strCharacters.toUtf8().data()
                                        << "</add>\n";
                        m_aOutputWriter.flush();
                    }
                    else
                    {
                        // TODO
                    }

                    m_nPositionOperationStart = m_nPositionLast;
                }
                else
                {
                    // TODO
                }

                if (m_nAutosaveCharacters > 0 &&
                    (m_nPositionOperationStart - nPositionCurrent) >= m_nAutosaveCharacters)
                {
                    if (m_aOutputWriter.is_open() == true &&
                        m_aOutputWriter.good() == true)
                    {
                        m_aOutputWriter << "<delete position=\"" << m_nPositionOperationStart << "\" count=\"" << (nPositionCurrent - m_nPositionOperationStart) << "\" />\n";
                        m_aOutputWriter.flush();
                    }
                    else
                    {
                        // TODO
                    }

                    m_nPositionOperationStart = -1;
                    m_nEditMode = EDITMODE_NONE;
                }
                else
                {
                    m_nEditMode = EDITMODE_DELETE;
                }

                m_strLastCharacterAdded = QString();
            }
        }
        else if (pEvent->key() == Qt::Key_Delete)
        {
            if (m_nTextLengthLast > nTextLengthCurrent)
            {
                if (m_nEditMode == EDITMODE_NONE)
                {
                    m_nPositionOperationStart = nPositionCurrent + (m_nTextLengthLast - nTextLengthCurrent);
                    m_nPositionLast = nPositionCurrent;
                }
                else if (m_nEditMode == EDITMODE_DELETE)
                {
                    // TODO: What if m_nPositionLast != nPositionCurrent?
                    m_nPositionLast = nPositionCurrent;
                    m_nPositionOperationStart += (m_nTextLengthLast - nTextLengthCurrent);
                }
                else if (m_nEditMode == EDITMODE_ADD)
                {
                    if (m_aOutputWriter.is_open() == true &&
                        m_aOutputWriter.good() == true)
                    {
                        m_aOutputWriter << "<add position=\"" << m_nPositionOperationStart + "\">";

                        QString strCharacters(toPlainText().mid(m_nPositionOperationStart, nPositionCurrent - m_nPositionOperationStart));

                        /*
                        if (m_strLastCharacterAdded.isEmpty() != true)
                        {
                            strCharacters += m_strLastCharacterAdded;
                        }
                        */

                        // Ampersand needs to be the first, otherwise it would double-encode
                        // other entities.
                        strCharacters.replace("&", "&amp;");
                        strCharacters.replace("<", "&lt;");
                        strCharacters.replace(">", "&gt;");

                        m_aOutputWriter << strCharacters.toUtf8().data()
                                        << "</add>\n";
                        m_aOutputWriter.flush();
                    }
                    else
                    {
                        // TODO
                    }

                    m_nPositionOperationStart = m_nPositionLast + (m_nTextLengthLast - nTextLengthCurrent);
                }
                else
                {
                    // TODO
                }

                if (m_nAutosaveCharacters > 0 &&
                    (m_nPositionOperationStart - nPositionCurrent) >= m_nAutosaveCharacters)
                {
                    if (m_aOutputWriter.is_open() == true &&
                        m_aOutputWriter.good() == true)
                    {
                        m_aOutputWriter << "<delete position=\"" << m_nPositionOperationStart << "\" count=\"" << (nPositionCurrent - m_nPositionOperationStart) << "\" />\n";
                        m_aOutputWriter.flush();
                    }
                    else
                    {
                        // TODO
                    }

                    m_nPositionOperationStart = -1;
                    m_nEditMode = EDITMODE_NONE;
                }
                else
                {
                    m_nEditMode = EDITMODE_DELETE;
                }

                m_strLastCharacterAdded = QString();
            }
        }
        else
        {
            // Don't rely on pEvent->key(), linebreaks 0xA vs. 0xD can
            // differ between the character(s) from toPlainText() and
            // the character(s) from pEvent->key(), probably even in
            // size, so hopefully all positions correspond with what's
            // extracted from toPlainText(). The default should be 0xA
            // everywhere, but then there would be the need to fix all
            // other linebreak variants and positions accordingly, which
            // remains as a potential TODO.
            if (m_nPositionLast < nPositionCurrent)
            {
                m_strLastCharacterAdded = toPlainText().mid(m_nPositionLast, nPositionCurrent - m_nPositionLast);
            }
            else
            {
                m_strLastCharacterAdded = toPlainText().mid(nPositionCurrent, m_nPositionLast - nPositionCurrent);
            }

            if (m_nEditMode == EDITMODE_NONE)
            {
                m_nPositionOperationStart = m_nPositionLast;
            }
            else if (m_nEditMode == EDITMODE_ADD)
            {
                // Ignore.
            }
            else if (m_nEditMode == EDITMODE_DELETE)
            {
                if (m_aOutputWriter.is_open() == true &&
                    m_aOutputWriter.good() == true)
                {
                    m_aOutputWriter << "<delete position=\"" << m_nPositionOperationStart << "\" count=\"" << (m_nPositionLast - m_nPositionOperationStart) << "\" />\n";
                    m_aOutputWriter.flush();
                }
                else
                {
                    // TODO
                }

                m_nPositionOperationStart = m_nPositionLast;
            }
            else
            {
                // TODO
            }

            if (m_nAutosaveCharacters > 0 &&
                (nPositionCurrent - m_nPositionOperationStart) >= m_nAutosaveCharacters)
            {
                if (m_aOutputWriter.is_open() == true &&
                    m_aOutputWriter.good() == true)
                {
                    m_aOutputWriter << "<add position=\"" << m_nPositionOperationStart << "\">";

                    QString strCharacters(toPlainText().mid(m_nPositionOperationStart, nPositionCurrent - m_nPositionOperationStart));

                    // Ampersand needs to be the first, otherwise it would double-encode
                    // other entities.
                    strCharacters.replace("&", "&amp;");
                    strCharacters.replace("<", "&lt;");
                    strCharacters.replace(">", "&gt;");

                    m_aOutputWriter << strCharacters.toUtf8().data()
                                    << "</add>\n";
                    m_aOutputWriter.flush();
                }
                else
                {
                    // TODO
                }

                m_nPositionOperationStart = -1;
                m_nEditMode = EDITMODE_NONE;
            }
            else
            {
                m_nEditMode = EDITMODE_ADD;
            }
        }
    }
    else
    {
        // Ignore.
    }

    m_nPositionLast = nPositionCurrent;
    m_nTextLengthLast = nTextLengthCurrent;
    m_bKeyPressEvent = false;
}

void TrackingTextEdit::closeEvent(QCloseEvent* pEvent)
{
    if (m_nEditMode == EDITMODE_ADD &&
        (m_nPositionLast - m_nPositionOperationStart) > 0)
    {
        if (m_aOutputWriter.is_open() == true &&
            m_aOutputWriter.good() == true)
        {
            m_aOutputWriter << "<add position=\"" << m_nPositionOperationStart << "\">";

            QString strCharacters(toPlainText().mid(m_nPositionOperationStart, m_nPositionLast - m_nPositionOperationStart));

            // Ampersand needs to be the first, otherwise it would double-encode
            // other entities.
            strCharacters.replace("&", "&amp;");
            strCharacters.replace("<", "&lt;");
            strCharacters.replace(">", "&gt;");

            m_aOutputWriter << strCharacters.toUtf8().data()
                            << "</add>\n";
            m_aOutputWriter.flush();
        }
        else
        {
            // TODO
        }
    }
    else if (m_nEditMode == EDITMODE_DELETE &&
             (m_nPositionOperationStart - m_nPositionLast) > 0)
    {
        if (m_aOutputWriter.is_open() == true &&
            m_aOutputWriter.good() == true)
        {
            m_aOutputWriter << "<delete position=\"" << m_nPositionOperationStart << "\" count=\"" << (m_nPositionLast - m_nPositionOperationStart) << "\" />\n";
            m_aOutputWriter.flush();
        }
        else
        {
            // TODO
        }
    }

    if (m_aOutputWriter.is_open() == true &&
        m_aOutputWriter.good() == true)
    {
        m_aOutputWriter << "<text-dump>"
                        << toPlainText().toUtf8().data()
                        << "</text-dump>\n";
        m_aOutputWriter.flush();
    }
    else
    {
        // TODO
    }

    if (m_aOutputWriter.is_open() == true &&
        m_aOutputWriter.good() == true)
    {
        m_aOutputWriter << "</change-tracking-text-editor-1-text-history>\n";
        m_aOutputWriter.flush();
    }
    else
    {
        // TODO
    }

    QPlainTextEdit::closeEvent(pEvent);
}

bool TrackingTextEdit::canInsertFromMimeData(const QMimeData*) const
{
    return false;
}

void TrackingTextEdit::insertFromMimeData(const QMimeData*)
{
    // Ignore.
}

void TrackingTextEdit::cursorPositionChanged()
{
    if (m_bKeyPressEvent == false)
    {
        if (m_nEditMode == EDITMODE_ADD)
        {
            if (m_aOutputWriter.is_open() == true &&
                m_aOutputWriter.good() == true)
            {
                m_aOutputWriter << "<add position=\"" << m_nPositionOperationStart << "\">";

                QString strCharacters(toPlainText().mid(m_nPositionOperationStart, m_nPositionLast - m_nPositionOperationStart));

                // Ampersand needs to be the first, otherwise it would double-encode
                // other entities.
                strCharacters.replace("&", "&amp;");
                strCharacters.replace("<", "&lt;");
                strCharacters.replace(">", "&gt;");

                m_aOutputWriter << strCharacters.toUtf8().data()
                                << "</add>\n";
                m_aOutputWriter.flush();
            }
            else
            {
                // TODO
            }
        }
        else if (m_nEditMode == EDITMODE_DELETE)
        {
            if (m_aOutputWriter.is_open() == true &&
                m_aOutputWriter.good() == true)
            {
                m_aOutputWriter << "<delete position=\"" << m_nPositionOperationStart << "\" count=\"" << (m_nPositionLast - m_nPositionOperationStart) << "\" />\n";
                m_aOutputWriter.flush();
            }
            else
            {
                // TODO
            }
        }

        m_nPositionLast = textCursor().position();
        m_nPositionOperationStart = -1;
        m_nEditMode = EDITMODE_NONE;
    }
}


/*
cursorPositionChanged
selectionChanged
textChanged
*/
