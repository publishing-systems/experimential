/* Copyright (C) 2018-2020 Stephan Kreutzer
 *
 * This file is part of change_tracking_text_editor_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * change_tracking_text_editor_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * change_tracking_text_editor_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with change_tracking_text_editor_1. If not, see <http://www.gnu.org/licenses/>.
 */



#include "TrackingTextEdit.h"
#include <QApplication>
#include <iostream>



int main(int argc, char* argv[])
{
    std::cout << "change_tracking_text_editor_1 Copyright (C) 2018-2020 Stephan Kreutzer\n"
              << "This program comes with ABSOLUTELY NO WARRANTY.\n"
              << "This is free software, and you are welcome to redistribute it\n"
              << "under certain conditions. See the GNU Affero General Public License 3\n"
              << "or any later version for details. Also, see the source code repository\n"
              << "https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/ and\n"
              << "the project website https://publishing-systems.org.\n"
              << std::endl;

    QApplication aApplication(argc, argv);

    TrackingTextEdit aTextEdit;
    aTextEdit.show();

    return aApplication.exec();
}
