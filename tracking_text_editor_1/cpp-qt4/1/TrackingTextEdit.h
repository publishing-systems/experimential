/* Copyright (C) 2018-2020 Stephan Kreutzer
 *
 * This file is part of change_tracking_text_editor_1, a submodule of the
 * digital_publishing_workflow_tools package.
 *
 * change_tracking_text_editor_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * change_tracking_text_editor_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with change_tracking_text_editor_1. If not, see <http://www.gnu.org/licenses/>.
 */



#include <QPlainTextEdit>
#include <fstream>



class TrackingTextEdit : public QPlainTextEdit
{
private:
    Q_OBJECT

public:
    TrackingTextEdit();

protected:
    virtual void keyPressEvent(QKeyEvent* pEvent);
    virtual void closeEvent(QCloseEvent* pEvent);
    virtual bool canInsertFromMimeData(const QMimeData* pSource) const;
    virtual void insertFromMimeData(const QMimeData* pSource);

protected slots:
    void cursorPositionChanged();

private:
    bool m_bKeyPressEvent;
    int m_nEditMode;
    int m_nAutosaveCharacters;
    int m_nPositionLast;
    int m_nPositionOperationStart;
    QString m_strLastCharacterAdded;
    int m_nTextLengthLast;
    std::ofstream m_aOutputWriter;

};
