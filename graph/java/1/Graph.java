/* Copyright (C) 2022-2024 Stephan Kreutzer
 *
 * This file is part of Graph.
 *
 * Graph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * Graph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with Graph. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/Graph.java
 * @author Stephan Kreutzer
 * @since 2022-04-11
 */

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.AbstractMap;

public class Graph
extends Frame
{
    public static void main(String[] args)
    {
        System.out.print("Graph Copyright (C) 2022-2024 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/experimental/ and\n" +
                         "the project website https://hypertext-systems.org.\n\n");

        Graph instance = new Graph(500, 400);
    }

    public Graph(int width, int height)
    {
        super("Graph");

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event)
            {
                closeWindow(event.getWindow());
            }
        });

        addMouseListener(new MouseClickHandler(this));
        addMouseMotionListener(new MouseMoveHandler(this));

        this.canvasWidth = width;
        this.canvasHeight = height;

        /*
        Map.Entry<List<GraphNode>, List<GraphEdge>> circle = Graph.generateCircle(100, 100, 75, 12, getCurrentNodeSize());

        for (int i = 0, max = circle.getKey().size(); i < max; i++)
        {
            addNode(circle.getKey().get(i));
        }

        for (int i = 0, max = circle.getValue().size(); i < max; i++)
        {
            addEdge(circle.getValue().get(i));
        }
        */

        setSize(this.canvasWidth, this.canvasHeight);
        setVisible(true);

        // No need to rely on overloaded paint().
        Graphics g = getGraphics();

        repaint();
    }

    public void closeWindow(Window window)
    {
        window.setVisible(false);
        window.dispose();
        System.exit(0);
    }

    public int addNode(GraphNode node)
    {
        this.nodes.add(node);
        return 0;
    }

    public int addEdge(GraphEdge edge)
    {
        this.edges.add(edge);
        return 0;
    }

    public List<GraphNode> getNodes()
    {
        return this.nodes;
    }

    public List<GraphEdge> getEdges()
    {
        return this.edges;
    }

    public boolean getDragging()
    {
        return this.dragging;
    }

    public int setDragging(boolean dragging)
    {
        this.dragging = dragging;
        return 0;
    }

    public double getCurrentNodeSize()
    {
        return this.nodeCircleRadius;
    }

    public int setCurrentNodeSize(int size)
    {
        this.nodeCircleRadius = size;
        return 0;
    }

    public int hitTest(double x, double y, GraphNode node)
    {
        double nodeX = node.getX();
        double nodeY = node.getY();
        double nodeRadius = node.getSize();

        // Hit test for point in circle.
        if ((((x - nodeX) * (x - nodeX)) +
             ((y - nodeY) * (y - nodeY))) <=
            (nodeRadius * nodeRadius))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public static Map.Entry<List<GraphNode>, List<GraphEdge>> generateCircle(int centerX,
                                                                             int centerY,
                                                                             double radius,
                                                                             int nodeCount,
                                                                             double nodeSize)
    {
        if (nodeCount < 2)
        {
            return null;
        }

        List<GraphNode> nodes = new ArrayList<GraphNode>();
        List<GraphEdge> edges = new ArrayList<GraphEdge>();

        double angle = 0.0;
        double stepAngle = 360 / nodeCount;

        for (int i = 0; i < nodeCount; i++)
        {
            double radians = angle * (Math.PI / 180.0);

            int x = centerX + (int)(radius * Math.cos(radians));
            int y = centerY + (int)(radius * Math.sin(radians));

            angle += stepAngle;

            nodes.add(new GraphNode(x, y, nodeSize));

            if (i > 0)
            {
                edges.add(new GraphEdge(nodes.get(i - 1), nodes.get(i)));
            }
        }

        if (nodeCount >= 2)
        {
            edges.add(new GraphEdge(nodes.get(nodeCount - 1), nodes.get(0)));
        }

        return new AbstractMap.SimpleEntry<List<GraphNode>, List<GraphEdge>>(nodes, edges);
    }

    // With this.getGraphics() no need to rely on overloaded paint().
    public void paint(Graphics g)
    {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, this.canvasWidth, this.canvasHeight);

        if (this.dragging == true)
        {
            this.dragging = false;
        }

        /** @todo Introduce partitioned stack for optimization! */

        for (int i = 0, max = this.edges.size(); i < max; i++)
        {
            render(g, this.edges.get(i));
        }

        for (int i = 0, max = this.nodes.size(); i < max; i++)
        {
            /** @todo Always draw selected nodes last to avoid overlap? */
            render(g, this.nodes.get(i));
        }
    }

    public int render(Graphics g, GraphNode node)
    {
        /*
        if (node.getDirty() != true)
        {
            return 1;
        }
        */

        if (node.getDoubleClicked() == true)
        {
            g.setColor(Color.YELLOW);
        }
        else if (node.getSelected() == true)
        {
            g.setColor(Color.RED);
        }
        else
        {
            g.setColor(Color.BLACK);
        }

        // Node is currently circle-shaped.
        g.fillOval(node.getX() - this.nodeCircleRadius,
                   node.getY() - this.nodeCircleRadius,
                   this.nodeCircleRadius * 2,
                   this.nodeCircleRadius * 2);

        if (node.getSelected() == true)
        {
            g.setColor(Color.BLACK);
        }

        node.setDirty(false);

        return 0;
    }

    public int render(Graphics g, GraphEdge edge)
    {
        /*
        if (edge.getDirty() != true)
        {
            return 1;
        }
        */

        if (edge.getSelected() == true)
        {
            g.setColor(Color.RED);
        }
        else
        {
            g.setColor(Color.BLACK);
        }

        // No "thickness" to the line (yet?).
        g.drawLine(edge.getFrom().getX(),
                   edge.getFrom().getY(),
                   edge.getTo().getX(),
                   edge.getTo().getY());

        if (edge.getSelected() == true)
        {
            g.setColor(Color.BLACK);
        }

        edge.setDirty(false);

        return 0;
    }

    protected class MouseClickHandler implements MouseListener
    {
        public MouseClickHandler(Graph parent)
        {
            this.parent = parent;
        }

        public void mousePressed(MouseEvent event)
        {
            final MouseEvent mouseEvent = (MouseEvent)event;

            if (mouseEvent.getButton() == MouseEvent.BUTTON1)
            {
                double x = mouseEvent.getX();
                double y = mouseEvent.getY();

                List<GraphNode> nodes = this.parent.getNodes();

                int hits = 0;

                for (int i = 0, max = nodes.size(); i < max; i++)
                {
                    GraphNode node = nodes.get(i);

                    if (this.parent.hitTest(x, y, node) != 0)
                    {
                        node.setSelected(!(node.getSelected()));

                        hits += 1;
                    }
                    else
                    {
                        node.setSelected(false);
                    }
                }

                repaint();
            }
        }

        public void mouseClicked(MouseEvent event)
        {
            final MouseEvent mouseEvent = (MouseEvent)event;

            if (mouseEvent.getButton() == MouseEvent.BUTTON1)
            {
                double x = mouseEvent.getX();
                double y = mouseEvent.getY();
                boolean doubleClick = mouseEvent.getClickCount() >= 2;

                List<GraphNode> nodes = this.parent.getNodes();

                List<GraphNode> nodesDoubleClickedOld = new ArrayList<GraphNode>();
                List<GraphNode> nodesSelectedCurrent = new ArrayList<GraphNode>();
                int hits = 0;

                for (int i = 0, max = nodes.size(); i < max; i++)
                {
                    GraphNode node = nodes.get(i);

                    if (node.getDoubleClicked() == true)
                    {
                        nodesDoubleClickedOld.add(node);
                    }

                    if (this.parent.hitTest(x, y, node) != 0)
                    {
                        hits += 1;

                        node.setDoubleClicked(doubleClick);

                        if (doubleClick == true)
                        {
                            node.setSelected(false);
                        }
                        else
                        {
                            if (node.getSelected() == true)
                            {
                                nodesSelectedCurrent.add(node);
                            }

                            node.setSelected(!(node.getSelected()));
                        }
                    }
                    else
                    {
                        node.setSelected(false);
                        node.setDoubleClicked(false);
                    }
                }

                if (hits == 0)
                {
                    GraphNode from = null;
                    // Currently, all circles for node representation created with the same radius.
                    GraphNode to = new GraphNode(mouseEvent.getX(), mouseEvent.getY(), this.parent.getCurrentNodeSize());

                    /*
                    if (nodes.size() > 0)
                    {
                        from = nodes.get(nodes.size() - 1);
                    }

                    if (from != null)
                    {
                        this.parent.addEdge(new GraphEdge(from, to));
                    }
                    */

                    this.parent.addNode(to);
                }
                else
                {
                    for (int i = 0, max = nodesDoubleClickedOld.size(); i < max; i++)
                    {
                        for (int j = 0, max2 = nodesSelectedCurrent.size(); j < max2; j++)
                        {
                            if (nodesDoubleClickedOld.contains(nodesSelectedCurrent.get(j)) != true)
                            {
                                /** @todo No protection yet against duplicate edges! Use duplication
                                    * for directionality? */
                                this.parent.addEdge(new GraphEdge(nodesDoubleClickedOld.get(i), nodesSelectedCurrent.get(j)));
                            }
                            else
                            {
                                // Self!
                            }
                        }
                    }
                }

                repaint();
            }
            else if (mouseEvent.getButton() == MouseEvent.BUTTON3)
            {
                double x = mouseEvent.getX();
                double y = mouseEvent.getY();

                List<GraphNode> nodes = this.parent.getNodes();
                List<GraphEdge> edges = this.parent.getEdges();

                List<GraphNode> nodesDoubleClickedOld = new ArrayList<GraphNode>();
                List<GraphNode> nodesSelectedCurrent = new ArrayList<GraphNode>();

                for (int i = 0, max = nodes.size(); i < max; i++)
                {
                    GraphNode node = nodes.get(i);

                    if (node.getDoubleClicked() == true)
                    {
                        nodesDoubleClickedOld.add(node);
                    }

                    if (this.parent.hitTest(x, y, node) != 0)
                    {
                        nodesSelectedCurrent.add(node);
                    }
                }

                if (nodesDoubleClickedOld.isEmpty() == true ||
                    nodesSelectedCurrent.isEmpty() == true)
                {
                    return;
                }

                List<GraphEdge> edgesRemove = new ArrayList<GraphEdge>();
                boolean nodeRemoval = false;

                for (int i = 0, max = nodesDoubleClickedOld.size(); i < max; i++)
                {
                    for (int j = 0, max2 = nodesSelectedCurrent.size(); j < max2; j++)
                    {
                        if (nodesDoubleClickedOld.get(i) == nodesSelectedCurrent.get(j))
                        {
                            GraphNode nodeRemove = nodesDoubleClickedOld.get(i);

                            nodes.remove(nodeRemove);
                            nodeRemoval = true;

                            for (int k = 0, max3 = edges.size(); k < max3; k++)
                            {
                                GraphEdge edge = edges.get(k);

                                if (edge.getFrom() == nodeRemove ||
                                    edge.getTo() == nodeRemove)
                                {
                                    edgesRemove.add(edge);
                                }
                            }
                        }
                    }
                }

                if (nodeRemoval == false)
                {
                    for (int i = 0, max = nodesDoubleClickedOld.size(); i < max; i++)
                    {
                        for (int j = 0, max2 = nodesSelectedCurrent.size(); j < max2; j++)
                        {
                            for (int k = 0, max3 = edges.size(); k < max3; k++)
                            {
                                GraphEdge edge = edges.get(k);

                                if ((edge.getFrom() == nodesDoubleClickedOld.get(i) &&
                                        edge.getTo() == nodesSelectedCurrent.get(j)) ||
                                    (edge.getFrom() == nodesSelectedCurrent.get(j) &&
                                        edge.getTo() == nodesDoubleClickedOld.get(i)))
                                {
                                    edgesRemove.add(edge);
                                }
                            }
                        }
                    }
                }

                for (int i = 0, max = edgesRemove.size(); i < max; i++)
                {
                    edges.remove(edgesRemove.get(i));
                }

                repaint();
            }
        }

        public void mouseExited(MouseEvent event)
        {

        }

        public void mouseEntered(MouseEvent event)
        {

        }

        public void mouseReleased(MouseEvent e)
        {

        }

        protected Graph parent = null;
    }

    protected class MouseMoveHandler implements MouseMotionListener
    {
        public MouseMoveHandler(Graph parent)
        {
            this.parent = parent;
        }

        public void mouseDragged(MouseEvent event)
        {
            final MouseEvent mouseEvent = (MouseEvent)event;

            //if (mouseEvent.getButton() == MouseEvent.BUTTON1)
            {
                int x = mouseEvent.getX();
                int y = mouseEvent.getY();

                List<GraphNode> nodes = this.parent.getNodes();

                boolean hasSelected = false;

                for (int i = 0, max = nodes.size(); i < max; i++)
                {
                    GraphNode node = nodes.get(i);

                    if (node.getSelected() == true)
                    {
                        /** @todo With multiple nodes selected, they all get the
                            * current mouse position, instead of a distance offset
                            * to their current position! Leads to "merge" effect. */
                        node.setX(x);
                        node.setY(y);

                        hasSelected = true;
                    }
                }

                if (hasSelected == true)
                {
                    this.parent.setDragging(true);

                    for (int i = 0, max = nodes.size(); i < max; i++)
                    {
                        nodes.get(i).setDirty(true);
                    }

                    List<GraphEdge> edges = this.parent.getEdges();

                    for (int i = 0, max = edges.size(); i < max; i++)
                    {
                        edges.get(i).setDirty(true);
                    }
                }

                repaint();
            }
        }

        public void mouseMoved(MouseEvent event)
        {

        }

        protected Graph parent = null;
    }

    protected List<GraphNode> nodes = new ArrayList<GraphNode>();
    protected List<GraphEdge> edges = new ArrayList<GraphEdge>();

    protected int canvasWidth = 500;
    protected int canvasHeight = 400;

    protected int nodeCircleRadius = 5;

    /** @todo Is probably not dragging-specific, so might (including methods)
      * be renamed to "clean"/"swipe" (background/all)? */
    protected boolean dragging = false;
}
