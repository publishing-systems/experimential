// Copyright (C) 2023-2024 Stephan Kreutzer
//
// This file is part of Graph.
//
// Graph is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License version 3 or any later version,
// as published by the Free Software Foundation.
//
// Graph is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License 3 for more details.
//
// You should have received a copy of the GNU Affero General Public License 3
// along with Graph. If not, see <http://www.gnu.org/licenses/>.


unit UGraph;

{$mode objfpc}{$H+}

// WARNING: Many assignments, even where coming from a var (reference)
// parameter, may create local copies into the members, which is 1.
// unnecessary and 2. not available for the address of the original
// instance, because address will be for the decoubled copy/duplicate.

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs;

type
TGraphNode = object
  public
    constructor Create(x, y, size: integer);
    function getX(): integer;
    function setX(x: integer): integer;
    function getY(): integer;
    function setY(y: integer): integer;
    function getSize(): integer;
    function setSize(size: integer): integer;
    function getDirty(): boolean;
    function setDirty(dirty: boolean): integer;
    function getSelected(): boolean;
    function setSelected(selected: boolean): integer;
    function getDoubleClicked(): boolean;
    function setDoubleClicked(doubleClicked: boolean): integer;
  protected
    var x, y: integer;
    var size: integer;
    var dirty, selected, doubleClicked: boolean;
end;

type TGraphNodePtr = ^TGraphNode;

type
TGraphEdge = object
  public
    constructor Create(ptrFromNode, ptrToNode: TGraphNodePtr);
    function getFromNode(): TGraphNodePtr;
    function getToNode(): TGraphNodePtr;
    function getDirty(): boolean;
    function setDirty(dirty: boolean): integer;
  protected
    var ptrFromNode, ptrToNode: TGraphNodePtr;
    var dirty: boolean;
end;

type TGraphEdgePtr = ^TGraphEdge;

type

  { TGraphForm }

  TGraphForm = class(TForm)
    // TODO: Check default scope
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormMouseDown(Sender: TObject;
                            Button: TMouseButton;
                            Shift: TShiftState;
                            X, Y: Integer);
    procedure FormMouseMove(Sender: TObject;
                            Shift: TShiftState;
                            X, Y: Integer);
    procedure FormMouseUp(Sender: TObject;
                          Button: TMouseButton;
                          Shift: TShiftState;
                          X, Y: Integer);
    procedure FormDblClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
  public
    function addNode(nodePtr: TGraphNodePtr): integer;
    function addEdge(edgePtr: TGraphEdgePtr): integer;
    function getCurrentNodeSize(): integer;
    function setCurrentNodeSize(size: integer): integer;
    function hitTest(x, y: integer; var node: TGraphNode): integer;
    function render(): integer;
    function renderNode(nodePtr: TGraphNodePtr): integer;
    function renderEdge(edgePtr: TGraphEdgePtr): integer;
    function fillCircle(centerX, centerY: integer;
                        radius: integer;
                        nodeColor: TColor): integer;
  protected
    var nodePtrs: array of ^TGraphNode;
    var edgePtrs: array of ^TGraphEdge;
    var redrawAll, myDragging: boolean;
    var nodeCircleRadius: integer;
  end;

var
  GraphForm: TGraphForm;

implementation

{
// Just because...ObjectPascal doesn't come with functions for manipulating arrays?!?
// https://forum.lazarus.freepascal.org/index.php?topic=45839.0
// but this is under Lazarus 2.0.6 and not 3.0.
generic function removeArrayItem<T>(var target: array of T;
                                    index: integer,
                                    freeMemory: boolean): integer;
var i: integer
begin
    if freeMemory = true then
    begin
        dispose(target[index]);
    end;

    for i := index + 1 to Length(target) - 1 do
    begin
        target[i - 1] := target[i];
    end;

    SetLength(target, Length(target) - 1);

    Exit(0);
end;
}

{$R *.lfm}

constructor TGraphNode.Create(x, y, size: integer);
begin
  self.x := x;
  self.y := y;
  self.size := size;
  self.dirty := true;
  self.selected := false;
  self.doubleClicked := false;
end;

function TGraphNode.getX(): integer;
begin
  getX := self.x;
end;

function TGraphNode.setX(x: integer): integer;
begin
  self.x := x;
  self.dirty := true;

  setX := 0;
end;

function TGraphNode.getY(): integer;
begin
  getY := self.y;
end;

function TGraphNode.setY(y: integer): integer;
begin
  self.y := y;
  self.dirty := true;

  setY := 0;
end;

function TGraphNode.getSize(): integer;
begin
  getSize := self.size;
end;

function TGraphNode.setSize(size: integer): integer;
begin
  self.size := size;
  self.dirty := true;

  setSize := 0;
end;

function TGraphNode.getDirty(): boolean;
begin
  getDirty := self.dirty;
end;

function TGraphNode.setDirty(dirty: boolean): integer;
begin
  self.dirty := dirty;
  setDirty := 0;
end;

function TGraphNode.getSelected(): boolean;
begin
  getSelected := self.selected;
end;

function TGraphNode.setSelected(selected: boolean): integer;
begin
  if (self.selected <> selected) then
  begin
      self.selected := selected;
      self.dirty := true;
      setSelected := 1
  end
  else
  begin
      setSelected := 0
  end;
end;

function TGraphNode.getDoubleClicked(): boolean;
begin
  getDoubleClicked := self.doubleClicked;
end;

function TGraphNode.setDoubleClicked(doubleClicked: boolean): integer;
begin
  if (self.doubleClicked <> doubleClicked) then
  begin
      self.doubleClicked := doubleClicked;
      self.dirty := true;
      setDoubleClicked := 1
  end
  else
  begin
      setDoubleClicked := 0
  end;
end;

constructor TGraphEdge.Create(ptrFromNode, ptrToNode: TGraphNodePtr);
begin
  self.ptrFromNode := ptrFromNode;
  self.ptrToNode := ptrToNode;
  self.dirty := true;
end;

function TGraphEdge.getFromNode(): TGraphNodePtr;
begin
  Exit(self.ptrFromNode);
end;

function TGraphEdge.getToNode(): TGraphNodePtr;
begin
  Exit(self.ptrToNode);
end;

function TGraphEdge.getDirty(): boolean;
begin
  getDirty := self.dirty;
end;

function TGraphEdge.setDirty(dirty: boolean): integer;
begin
  self.dirty := dirty;
  setDirty := 0;
end;

procedure TGraphForm.FormCreate(Sender: TObject);
begin
  self.redrawAll := true;
  self.nodeCircleRadius := 5;
  self.myDragging := false;
end;

procedure TGraphForm.FormDestroy(Sender: TObject);
var i: integer;
begin
  for i := 0 to Length(self.edgePtrs) - 1 do
  begin
    dispose(self.edgePtrs[i]);
  end;

  for i := 0 to Length(self.nodePtrs) - 1 do
  begin
    dispose(self.nodePtrs[i]);
  end;
end;

procedure TGraphForm.FormMouseDown(Sender: TObject;
                                   Button: TMouseButton;
                                   Shift: TShiftState;
                                   X, Y: Integer);
var nodesDoubleClickedOldPtr: array of ^TGraphNode;
var nodesSelectedCurrentPtr: array of ^TGraphNode;
var hits: integer;
var i, j, k, l, max: integer;
var edgePtr: ^TGraphEdge;
var found, removal: boolean;
var ptrFromNode, ptrToNode: TGraphNodePtr;
begin
  if (Button = TMouseButton.mbLeft) then
  begin
      hits := 0;

      for i := 0 to Length(self.nodePtrs) - 1 do
      begin
          if (self.nodePtrs[i]^.getDoubleClicked() = true) then
          begin
              SetLength(nodesDoubleClickedOldPtr, Length(nodesDoubleClickedOldPtr) + 1);
              nodesDoubleClickedOldPtr[Length(nodesDoubleClickedOldPtr) - 1] := self.nodePtrs[i];
          end;

          if (hitTest(X, Y, self.nodePtrs[i]^) <> 0) then
          begin
              if (self.nodePtrs[i]^.getSelected() <> true) then
              begin
                  self.nodePtrs[i]^.setSelected(true);

                  SetLength(nodesSelectedCurrentPtr, Length(nodesSelectedCurrentPtr) + 1);
                  nodesSelectedCurrentPtr[Length(nodesSelectedCurrentPtr) - 1] := self.nodePtrs[i];

                  self.myDragging := true;
              end;

              hits += 1;
          end
          else
          begin
              self.nodePtrs[i]^.setSelected(false);
              self.nodePtrs[i]^.setDoubleClicked(false);
          end;
      end;

      if (hits > 0) then
      begin
          for i := 0 to Length(nodesDoubleClickedOldPtr) - 1 do
          begin
              for j := 0 to Length(nodesSelectedCurrentPtr) - 1 do
              begin
                  // Error: Operator is not overloaded.
                  //if (nodesSelectedCurrent[j] in nodesDoubleClickedOld) then

                  found := false;

                  for k := 0 to Length(nodesDoubleClickedOldPtr) - 1 do
                  begin
                      if (nodesDoubleClickedOldPtr[k] = nodesSelectedCurrentPtr[j]) then
                      begin
                          found := true;
                          break;
                      end;
                  end;

                  if (found <> true) then
                  begin
                      // TODO: No protection yet against duplicate edges! Duplication to use for directionality?

                      new(edgePtr);
                      edgePtr^.Create(nodesDoubleClickedOldPtr[i], nodesSelectedCurrentPtr[j]);

                      SetLength(self.edgePtrs, Length(self.edgePtrs) + 1);
                      self.edgePtrs[Length(self.edgePtrs) - 1] := edgePtr;

                      edgePtr^.getFromNode()^.setSelected(false);
                      edgePtr^.getFromNode()^.setDoubleClicked(false);
                      edgePtr^.getToNode()^.setSelected(false);
                      edgePtr^.getToNode()^.setDoubleClicked(false);

                      self.myDragging := false;
                  end
                  else
                  begin
                      // Self!
                  end;
              end;
          end;

          render();
      end;
  end
  else if (Button = TMouseButton.mbRight) then
  begin
    // Some of this might be duplicated in TGraphForm.FormMouseUp(),
    // probably to make sure this works irrespective of event order/behavior.

    hits := 0;

    for i := 0 to Length(self.nodePtrs) - 1 do
    begin
        if (self.nodePtrs[i]^.getDoubleClicked() = true) then
        begin
            SetLength(nodesDoubleClickedOldPtr, Length(nodesDoubleClickedOldPtr) + 1);
            nodesDoubleClickedOldPtr[Length(nodesDoubleClickedOldPtr) - 1] := self.nodePtrs[i];
        end;

        if (hitTest(X, Y, self.nodePtrs[i]^) <> 0) then
        begin
            if (self.nodePtrs[i]^.getSelected() <> true) then
            begin
                SetLength(nodesSelectedCurrentPtr, Length(nodesSelectedCurrentPtr) + 1);
                nodesSelectedCurrentPtr[Length(nodesSelectedCurrentPtr) - 1] := self.nodePtrs[i];
            end;

            hits += 1;
        end
        else
        begin
            self.nodePtrs[i]^.setSelected(false);
            self.nodePtrs[i]^.setDoubleClicked(false);
        end;
    end;

    if (hits > 0) then
    begin
        for i := 0 to Length(nodesDoubleClickedOldPtr) - 1 do
        begin
            for j := 0 to Length(nodesSelectedCurrentPtr) - 1 do
            begin
                // Error: Operator is not overloaded.
                //if (nodesSelectedCurrent[j] in nodesDoubleClickedOld) then

                found := false;

                for k := 0 to Length(nodesDoubleClickedOldPtr) - 1 do
                begin
                    if (nodesDoubleClickedOldPtr[k] = nodesSelectedCurrentPtr[j]) then
                    begin
                        found := true;
                        break;
                    end;
                end;

                if (found = true) then
                begin
                  removal := false;

                  k := 0;
                  max := Length(self.edgePtrs) - 1;

                  while (k <= max) do
                  begin
                    ptrFromNode := self.edgePtrs[k]^.getFromNode();
                    ptrToNode := self.edgePtrs[k]^.getToNode();

                    if (((ptrFromNode = nodesDoubleClickedOldPtr[i]) and
                         (ptrToNode = nodesSelectedCurrentPtr[j])) or
                        ((ptrFromNode = nodesSelectedCurrentPtr[j]) and
                         (ptrToNode = nodesDoubleClickedOldPtr[i]))) then
                    begin
                      //removeArrayItem(self.edgesPtr, k, true);
                      dispose(self.edgePtrs[k]);
                      for l := k + 1 to Length(self.edgePtrs) - 1 do
                      begin
                          self.edgePtrs[l - 1] := self.edgePtrs[l];
                      end;
                      SetLength(self.edgePtrs, Length(self.edgePtrs) - 1);

                      max -= 1;
                      k -= 1;

                      removal := true;
                    end;

                    k += 1;
                  end;

                  if (removal = true) then
                  begin
                    self.myDragging := true;
                    self.redrawAll := true;
                  end;
                end
                else
                begin
                    // Self!
                end;
            end;
        end;

        render();
    end;
  end;
end;

procedure TGraphForm.FormMouseMove(Sender: TObject;
                                   Shift: TShiftState;
                                   X, Y: Integer);
var i: integer;
var found: boolean;
begin
  if (self.myDragging = true) then
  begin
    found := false;

    for i := 0 to Length(self.nodePtrs) - 1 do
    begin
      if (self.nodePtrs[i]^.getSelected() = true) then
      begin
        self.nodePtrs[i]^.setX(X);
        self.nodePtrs[i]^.setY(Y);

        found := true;
      end;
    end;

    if (found = true) then
    begin
      self.redrawAll := true;
      render();
    end;
  end;
end;

procedure TGraphForm.FormMouseUp(Sender: TObject;
                                 Button: TMouseButton;
                                 Shift: TShiftState;
                                 X, Y: Integer);
var hits: integer;
var i, j, k: integer;
var nodePtr: ^TGraphNode;
var ptrFromNode, ptrToNode: TGraphNodePtr;
var nodesRemovedPtrs: array of ^TGraphNode;
var max: integer;
var found: boolean;
begin
  if (self.myDragging = true) then
  begin
    for i := 0 to Length(self.nodePtrs) - 1 do
    begin
        self.nodePtrs[i]^.setSelected(false);
    end;

    self.myDragging := false;
    self.redrawAll := true;
  end
  else
  begin
    if (Button = TMouseButton.mbLeft) then
    begin
      hits := 0;

      for i := 0 to Length(self.nodePtrs) - 1 do
      begin
        if (hitTest(X, Y, self.nodePtrs[i]^) <> 0) then
        begin
            hits += 1;
        end;

        // Here, double click event doesn't eat up the mouse-up event,
        // so removing doubleclick state on all prevents edges from being
        // created.
        //self.nodePtrs[i]^.setSelected(false);
        //self.nodePtrs[i]^.setDoubleClicked(false);
      end;

      if (hits <= 0) then
      begin
          new(nodePtr);
          nodePtr^.Create(X, Y, self.nodeCircleRadius);
          SetLength(self.nodePtrs, Length(self.nodePtrs) + 1);
          self.nodePtrs[Length(self.nodePtrs) - 1] := nodePtr;

          render();
      end;
    end
    else if (Button = TMouseButton.mbRight) then
    begin
      // Some of this might be duplicated in TGraphForm.FormMouseDown(),
      // probably to make sure this works irrespective of event order/behavior.

      for i := 0 to Length(self.nodePtrs) - 1 do
      begin
        if ((hitTest(X, Y, self.nodePtrs[i]^) <> 0) and
            (self.nodePtrs[i]^.getDoubleClicked() = true)) then
        begin
          SetLength(nodesRemovedPtrs, Length(nodesRemovedPtrs) + 1);
          nodesRemovedPtrs[Length(nodesRemovedPtrs) - 1] := self.nodePtrs[i];
        end;
      end;

      if (Length(nodesRemovedPtrs) > 0) then
      begin
          i := 0;
          max := Length(self.edgePtrs) - 1;

          while (i <= max) do
          begin
            for j := 0 to Length(nodesRemovedPtrs) - 1 do
            begin
              ptrFromNode := self.edgePtrs[i]^.getFromNode();
              ptrToNode := self.edgePtrs[i]^.getToNode();

              if ((ptrFromNode = nodesRemovedPtrs[j]) or
                  (ptrToNode = nodesRemovedPtrs[j])) then
              begin

                //removeArrayItem(self.edgePtrs, i, true);
                dispose(self.edgePtrs[i]);
                for k := i + 1 to Length(self.edgePtrs) - 1 do
                begin
                    self.edgePtrs[k - 1] := self.edgePtrs[k];
                end;
                SetLength(self.edgePtrs, Length(self.edgePtrs) - 1);

                max -= 1;
                i -= 1;

                break;
              end;
            end;

            i += 1;
          end;

          for i := 0 to Length(nodesRemovedPtrs) - 1 do
          begin
            found := false;

            for j := 0 to Length(self.nodePtrs) - 1 do
            begin
              if nodesRemovedPtrs[i] = self.nodePtrs[j] then
              begin
                found := true;

                //removeArrayItem(self.nodePtrs, j, true);
                dispose(self.nodePtrs[j]);
                for k := j + 1 to Length(self.nodePtrs) - 1 do
                begin
                    self.nodePtrs[k - 1] := self.nodePtrs[k];
                end;
                SetLength(self.nodePtrs, Length(self.nodePtrs) - 1);

                break;
              end;
            end;

            if found = true then
            begin
              // Error!
            end;
          end;

          self.redrawAll := true;
          render();
      end;
    end;
  end;
end;

procedure TGraphForm.FormDblClick(Sender: TObject);
var X, Y: integer;
var i: integer;
var isDoubleClicked: boolean;
begin
  X := ScreenToClient(Mouse.CursorPos).x;
  Y := ScreenToClient(Mouse.CursorPos).y;

  isDoubleClicked := false;

  for i := 0 to Length(self.nodePtrs) - 1 do
  begin
    if (hitTest(X, Y, self.nodePtrs[i]^) <> 0) then
    begin
      self.nodePtrs[i]^.setDoubleClicked(true);
      isDoubleClicked := true;
    end;

    self.nodePtrs[i]^.setSelected(false);
  end;

  self.myDragging := false;

  if (isDoubleClicked = true) then
  begin
    render();
  end;
end;

procedure TGraphForm.FormPaint(Sender: TObject);
begin
  self.redrawAll := true;
  render();
end;

function TGraphForm.addNode(nodePtr: TGraphNodePtr): integer;
begin
  SetLength(self.nodePtrs, Length(self.nodePtrs) + 1);
  self.nodePtrs[Length(self.nodePtrs) - 1] := nodePtr;
  addNode := 0;
end;

function TGraphForm.addEdge(edgePtr: TGraphEdgePtr): integer;
begin
  SetLength(self.edgePtrs, Length(self.edgePtrs) + 1);
  self.edgePtrs[Length(self.edgePtrs) - 1] := edgePtr;
  addEdge := 0;
end;

function TGraphForm.getCurrentNodeSize(): integer;
begin
  getCurrentNodeSize := self.nodeCircleRadius;
end;

function TGraphForm.setCurrentNodeSize(size: integer): integer;
begin
  self.nodeCircleRadius := size;
  setCurrentNodeSize := 0;
end;

function TGraphForm.hitTest(x, y: integer; var node: TGraphNode): integer;
var nodeX, nodeY: integer;
var nodeRadius: integer;
begin
  nodeX := node.getX();
  nodeY := node.getY();
  nodeRadius := node.getSize();

  // Hit test for point in circle.
  if ((((x - nodeX) * (x - nodeX)) +
       ((y - nodeY) * (y - nodeY))) <=
      (nodeRadius * nodeRadius)) then
  begin
      Exit(1);
  end
  else
  begin
      Exit(0);
  end;
end;

function TGraphForm.render(): integer;
var i: integer;
begin
  if (self.redrawAll = true) then
  begin
      Canvas.Brush.Color := clWhite;
      Canvas.Pen.Color := clWhite;
      Canvas.Rectangle(0, 0, Canvas.Width, Canvas.Height);

      for i := 0 to Length(self.nodePtrs) - 1 do
      begin
        self.nodePtrs[i]^.setDirty(true);
      end;

      for i := 0 to Length(self.edgePtrs) - 1 do
      begin
        self.edgePtrs[i]^.setDirty(true);
      end;

      self.redrawAll := false;
  end;

  // TODO: Introduce partitioned stack for optimization!

  for i := 0 to Length(self.edgePtrs) - 1 do
  begin
    renderEdge(self.edgePtrs[i]);
  end;

  for i := 0 to Length(self.nodePtrs) - 1 do
  begin
    // TODO: Always draw selected nodes last to avoid overlap?
    renderNode(self.nodePtrs[i]);
  end;

  Exit(0);
end;

function TGraphForm.renderNode(nodePtr: TGraphNodePtr): integer;
var nodeColor: TColor;
begin
  if (nodePtr^.getDirty() <> true) then
  begin
      Exit(1);
  end;

  nodeColor := clBlack;

  if (nodePtr^.getSelected() = true) then
  begin
      nodeColor := clRed;
  end
  else if (nodePtr^.getDoubleClicked() = true) then
  begin
      nodeColor := clYellow;
  end;

  fillCircle(nodePtr^.getX(),
             nodePtr^.getY(),
             nodePtr^.getSize(),
             nodeColor);

  nodePtr^.setDirty(false);

  Exit(0);
end;

function TGraphForm.renderEdge(edgePtr: TGraphEdgePtr): integer;
begin
  if (edgePtr^.getDirty() <> true) then
  begin
    Exit(1);
  end;

  Canvas.Pen.Color := clBlack;

  // No "thickness" to the line (yet?).
  Canvas.Line(edgePtr^.getFromNode()^.getX(),
              edgePtr^.getFromNode()^.getY(),
              edgePtr^.getToNode()^.getX(),
              edgePtr^.getToNode()^.getY());

  edgePtr^.setDirty(false);

  Exit(0);
end;

function TGraphForm.fillCircle(centerX, centerY: integer;
                               radius: integer;
                               nodeColor: TColor): integer;
begin
  Canvas.Brush.Color := nodeColor;
  Canvas.Pen.Color := nodeColor;
  Canvas.Ellipse(centerX - radius,
                 centerY - radius,
                 centerX + radius,
                 centerY + radius);
  Exit(0);
end;

end.

