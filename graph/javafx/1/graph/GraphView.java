/* Copyright (C) 2022 Stephan Kreutzer
 *
 * This file is part of Graph.
 *
 * Graph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * Graph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with Graph. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/GraphView.java
 * @author Stephan Kreutzer
 * @since 2022-04-11
 */

import javafx.scene.layout.*;
import javafx.scene.canvas.*;
import javafx.scene.input.*;
import javafx.scene.paint.Color;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.AbstractMap;

public class GraphView
{
    public GraphView(Pane parent, double width, double height)
    {
        this.canvasWidth = width;
        this.canvasHeight = height;

        Canvas canvas = new Canvas(this.canvasWidth, this.canvasHeight);
        canvas.addEventHandler(InputEvent.ANY, new InputHandler(this));
        parent.getChildren().add(canvas);

        this.gc = canvas.getGraphicsContext2D();

        final AnimationTimer timer = new RedrawTimer(this);
        timer.start();
    }

    public int addNode(GraphNode node)
    {
        this.nodes.add(node);
        return 0;
    }

    public int addEdge(GraphEdge edge)
    {
        this.edges.add(edge);
        return 0;
    }

    public List<GraphNode> getNodes()
    {
        return this.nodes;
    }

    public List<GraphEdge> getEdges()
    {
        return this.edges;
    }

    public boolean getDragging()
    {
        return this.dragging;
    }

    public int setDragging(boolean dragging)
    {
        this.dragging = dragging;
        return 0;
    }

    public double getCurrentNodeSize()
    {
        return this.nodeCircleRadius;
    }

    public int setCurrentNodeSize(double size)
    {
        this.nodeCircleRadius = size;
        return 0;
    }

    public int hitTest(double x, double y, GraphNode node)
    {
        double nodeX = node.getX();
        double nodeY = node.getY();
        double nodeRadius = node.getSize();

        // Hit test for point in circle.
        if ((((x - nodeX) * (x - nodeX)) +
             ((y - nodeY) * (y - nodeY))) <=
            (nodeRadius * nodeRadius))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public static Map.Entry<List<GraphNode>, List<GraphEdge>> generateCircle(double centerX,
                                                                             double centerY,
                                                                             double radius,
                                                                             int nodeCount,
                                                                             double nodeSize)
    {
        if (nodeCount < 2)
        {
            return null;
        }

        List<GraphNode> nodes = new ArrayList<GraphNode>();
        List<GraphEdge> edges = new ArrayList<GraphEdge>();

        double angle = 0.0;
        double stepAngle = 360 / nodeCount;

        for (int i = 0; i < nodeCount; i++)
        {
            double radians = angle * (Math.PI / 180.0);

            double x = centerX + (radius * Math.cos(radians));
            double y = centerY + (radius * Math.sin(radians));

            angle += stepAngle;

            nodes.add(new GraphNode(x, y, nodeSize));

            if (i > 0)
            {
                edges.add(new GraphEdge(nodes.get(i - 1), nodes.get(i)));
            }
        }

        if (nodeCount >= 2)
        {
            edges.add(new GraphEdge(nodes.get(nodeCount - 1), nodes.get(0)));
        }

        return new AbstractMap.SimpleEntry<List<GraphNode>, List<GraphEdge>>(nodes, edges);
    }

    public int render()
    {
        if (this.dragging == true)
        {
            this.gc.setFill(Color.WHITE);
            this.gc.fillRect(0, 0, this.canvasWidth, this.canvasHeight);

            this.dragging = false;
        }

        /** @todo Introduce partitioned stack for optimization! */

        for (int i = 0, max = this.edges.size(); i < max; i++)
        {
            render(this.edges.get(i));
        }

        for (int i = 0, max = this.nodes.size(); i < max; i++)
        {
            /** @todo Always draw selected nodes last to avoid overlap? */
            render(this.nodes.get(i));
        }

        return 0;
    }

    public int render(GraphNode node)
    {
        if (node.getDirty() != true)
        {
            return 1;
        }

        if (node.getDoubleClicked() == true)
        {
            this.gc.setFill(Color.YELLOW);
        }
        else if (node.getSelected() == true)
        {
            this.gc.setFill(Color.RED);
        }
        else
        {
            this.gc.setFill(Color.BLACK);
        }

        // Node is currently circle-shaped.
        this.gc.fillOval(node.getX() - this.nodeCircleRadius,
                         node.getY() - this.nodeCircleRadius,
                         this.nodeCircleRadius * 2.0,
                         this.nodeCircleRadius * 2.0);

        if (node.getSelected() == true)
        {
            this.gc.setFill(Color.BLACK);
        }

        node.setDirty(false);

        return 0;
    }

    public int render(GraphEdge edge)
    {
        if (edge.getDirty() != true)
        {
            return 1;
        }

        if (edge.getSelected() == true)
        {
            this.gc.setStroke(Color.RED);
        }

        // No "thickness" to the line (yet?).
        this.gc.strokeLine(edge.getFrom().getX(),
                           edge.getFrom().getY(),
                           edge.getTo().getX(),
                           edge.getTo().getY());

        if (edge.getSelected() == true)
        {
            this.gc.setFill(Color.BLACK);
        }

        edge.setDirty(false);

        return 0;
    }

    /** @todo Pause if there's no new input? */
    protected class RedrawTimer extends AnimationTimer
    {
        public RedrawTimer(GraphView parent)
        {
            this.parent = parent;
        }

        @Override
        public void handle(long now)
        {
            this.parent.render();
        }

        protected GraphView parent = null;
    }

    protected class InputHandler implements EventHandler<InputEvent>
    {
        public InputHandler(GraphView parent)
        {
            this.parent = parent;
        }

        public void handle(InputEvent event)
        {
            if (event.getEventType() == MouseEvent.MOUSE_PRESSED)
            {
                final MouseEvent mouseEvent = (MouseEvent)event;

                if (mouseEvent.getButton().equals(MouseButton.PRIMARY) == true)
                {
                    double x = mouseEvent.getX();
                    double y = mouseEvent.getY();

                    List<GraphNode> nodes = this.parent.getNodes();

                    int hits = 0;

                    for (int i = 0, max = nodes.size(); i < max; i++)
                    {
                        GraphNode node = nodes.get(i);

                        if (this.parent.hitTest(x, y, node) != 0)
                        {
                            node.setSelected(!(node.getSelected()));

                            hits += 1;
                        }
                        else
                        {
                            node.setSelected(false);
                        }
                    }
                }
            }
            else if (event.getEventType() == MouseEvent.MOUSE_DRAGGED)
            {
                final MouseEvent mouseEvent = (MouseEvent)event;

                if (mouseEvent.getButton().equals(MouseButton.PRIMARY) == true)
                {
                    double x = mouseEvent.getX();
                    double y = mouseEvent.getY();

                    List<GraphNode> nodes = this.parent.getNodes();

                    boolean hasSelected = false;

                    for (int i = 0, max = nodes.size(); i < max; i++)
                    {
                        GraphNode node = nodes.get(i);

                        if (node.getSelected() == true)
                        {
                            /** @todo With multiple nodes selected, they all get the
                              * current mouse position, instead of a distance offset
                              * to their current position! Leads to "merge" effect. */
                            node.setX(x);
                            node.setY(y);

                            hasSelected = true;
                        }
                    }

                    if (hasSelected == true)
                    {
                        this.parent.setDragging(true);

                        for (int i = 0, max = nodes.size(); i < max; i++)
                        {
                            nodes.get(i).setDirty(true);
                        }

                        List<GraphEdge> edges = this.parent.getEdges();

                        for (int i = 0, max = edges.size(); i < max; i++)
                        {
                            edges.get(i).setDirty(true);
                        }
                    }
                }
            }
            else if (event.getEventType() == MouseEvent.MOUSE_RELEASED)
            {

            }
            else if (event.getEventType() == MouseEvent.MOUSE_CLICKED)
            {
                final MouseEvent mouseEvent = (MouseEvent)event;

                if (mouseEvent.getButton().equals(MouseButton.PRIMARY) == true)
                {
                    double x = mouseEvent.getX();
                    double y = mouseEvent.getY();
                    boolean doubleClick = mouseEvent.getClickCount() >= 2;

                    List<GraphNode> nodes = this.parent.getNodes();

                    List<GraphNode> nodesDoubleClickedOld = new ArrayList<GraphNode>();
                    List<GraphNode> nodesSelectedCurrent = new ArrayList<GraphNode>();
                    int hits = 0;

                    for (int i = 0, max = nodes.size(); i < max; i++)
                    {
                        GraphNode node = nodes.get(i);

                        if (node.getDoubleClicked() == true)
                        {
                            nodesDoubleClickedOld.add(node);
                        }

                        if (this.parent.hitTest(x, y, node) != 0)
                        {
                            hits += 1;

                            node.setDoubleClicked(doubleClick);

                            if (doubleClick == true)
                            {
                                node.setSelected(false);
                            }
                            else
                            {
                                if (node.getSelected() == true)
                                {
                                    nodesSelectedCurrent.add(node);
                                }

                                node.setSelected(!(node.getSelected()));
                            }
                        }
                        else
                        {
                            node.setSelected(false);
                            node.setDoubleClicked(false);
                        }
                    }

                    if (hits == 0)
                    {
                        GraphNode from = null;
                        // Currently, all circles for node representation created with the same radius.
                        GraphNode to = new GraphNode(mouseEvent.getX(), mouseEvent.getY(), this.parent.getCurrentNodeSize());

                        /*
                        if (nodes.size() > 0)
                        {
                            from = nodes.get(nodes.size() - 1);
                        }

                        if (from != null)
                        {
                            this.parent.addEdge(new GraphEdge(from, to));
                        }
                        */

                        this.parent.addNode(to);
                    }
                    else
                    {
                        for (int i = 0, max = nodesDoubleClickedOld.size(); i < max; i++)
                        {
                            for (int j = 0, max2 = nodesSelectedCurrent.size(); j < max2; j++)
                            {
                                if (nodesDoubleClickedOld.contains(nodesSelectedCurrent.get(j)) != true)
                                {
                                    /** @todo No protection yet against duplicate edges! Use duplication
                                      * for directionality? */
                                    this.parent.addEdge(new GraphEdge(nodesDoubleClickedOld.get(i), nodesSelectedCurrent.get(j)));
                                }
                                else
                                {
                                    // Self!
                                }
                            }
                        }
                    }
                }
                else if (mouseEvent.getButton().equals(MouseButton.SECONDARY) == true)
                {
                    double x = mouseEvent.getX();
                    double y = mouseEvent.getY();

                    List<GraphNode> nodes = this.parent.getNodes();
                    List<GraphEdge> edges = this.parent.getEdges();

                    List<GraphNode> nodesDoubleClickedOld = new ArrayList<GraphNode>();
                    List<GraphNode> nodesSelectedCurrent = new ArrayList<GraphNode>();

                    for (int i = 0, max = nodes.size(); i < max; i++)
                    {
                        GraphNode node = nodes.get(i);

                        if (node.getDoubleClicked() == true)
                        {
                            nodesDoubleClickedOld.add(node);
                        }

                        if (this.parent.hitTest(x, y, node) != 0)
                        {
                            nodesSelectedCurrent.add(node);
                        }
                    }

                    if (nodesDoubleClickedOld.isEmpty() == true ||
                        nodesSelectedCurrent.isEmpty() == true)
                    {
                        return;
                    }

                    List<GraphEdge> edgesRemove = new ArrayList<GraphEdge>();
                    boolean nodeRemoval = false;

                    for (int i = 0, max = nodesDoubleClickedOld.size(); i < max; i++)
                    {
                        for (int j = 0, max2 = nodesSelectedCurrent.size(); j < max2; j++)
                        {
                            if (nodesDoubleClickedOld.get(i) == nodesSelectedCurrent.get(j))
                            {
                                GraphNode nodeRemove = nodesDoubleClickedOld.get(i);

                                nodes.remove(nodeRemove);
                                nodeRemoval = true;

                                for (int k = 0, max3 = edges.size(); k < max3; k++)
                                {
                                    GraphEdge edge = edges.get(k);

                                    if (edge.getFrom() == nodeRemove ||
                                        edge.getTo() == nodeRemove)
                                    {
                                        edgesRemove.add(edge);
                                    }
                                }
                            }
                        }
                    }

                    if (nodeRemoval == false)
                    {
                        for (int i = 0, max = nodesDoubleClickedOld.size(); i < max; i++)
                        {
                            for (int j = 0, max2 = nodesSelectedCurrent.size(); j < max2; j++)
                            {
                                for (int k = 0, max3 = edges.size(); k < max3; k++)
                                {
                                    GraphEdge edge = edges.get(k);

                                    if ((edge.getFrom() == nodesDoubleClickedOld.get(i) &&
                                         edge.getTo() == nodesSelectedCurrent.get(j)) ||
                                        (edge.getFrom() == nodesSelectedCurrent.get(j) &&
                                         edge.getTo() == nodesDoubleClickedOld.get(i)))
                                    {
                                        edgesRemove.add(edge);
                                    }
                                }
                            }
                        }
                    }

                    for (int i = 0, max = edgesRemove.size(); i < max; i++)
                    {
                        edges.remove(edgesRemove.get(i));
                    }


                    // Lazy hack to force complete redraw.

                    this.parent.setDragging(true);

                    for (int i = 0, max = nodes.size(); i < max; i++)
                    {
                        nodes.get(i).setDirty(true);
                    }

                    for (int i = 0, max = edges.size(); i < max; i++)
                    {
                        edges.get(i).setDirty(true);
                    }
                }
            }
        }

        protected GraphView parent = null;
    }

    protected List<GraphNode> nodes = new ArrayList<GraphNode>();
    protected List<GraphEdge> edges = new ArrayList<GraphEdge>();

    protected double canvasWidth = 500.0;
    protected double canvasHeight = 400.0;

    protected double nodeCircleRadius = 5.0;

    protected GraphicsContext gc = null;

    /** @todo Is probably not dragging-specific, so might (including methods)
      * be renamed to "clean"/"swipe" (background/all)? */
    protected boolean dragging = false;
}
