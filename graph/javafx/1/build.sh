#!/bin/sh

# sudo apt-get install default-jdk openjfx

rm ./graph/*.class
javac -encoding UTF-8 --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls ./graph/*.java
