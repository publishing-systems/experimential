<?php
/* Copyright (C) 2024 Stephan Kreutzer
 *
 * This file is part of graph-world.
 *
 * graph-world is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * graph-world is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with graph-world. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/libraries/column_type_defines.inc.php
 * @author Stephan Kreutzer
 * @since 2023-05-14
 */



define("COLUMN_TYPE_VARCHAR", 1);
define("COLUMN_TYPE_BOOLEAN", 2);
define("COLUMN_TYPE_INTEGER", 3);

define("COLUMN_TYPES", array(COLUMN_TYPE_VARCHAR,
                             COLUMN_TYPE_BOOLEAN,
                             COLUMN_TYPE_INTEGER));



?>
