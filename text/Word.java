/* Copyright (C) 2021  Stephan Kreutzer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file
 * @author Stephan Kreutzer
 * @since 2021-07-23
 */


import java.util.Map;
import java.util.HashMap;


public class Word
{
    public Word(long wordIndex)
    {
        this.wordIndex = wordIndex;
    }

    public String GetStringCurrent()
    {
        return string.toString();
    }

    public StringBuilder GetString()
    {
        return string;
    }

    public Map<String, Long> GetCharacters()
    {
        return characters;
    }

    protected long wordIndex = 0L;
    protected StringBuilder string = new StringBuilder();
    protected Map<String, Long> characters = new HashMap<String, Long>();
}
