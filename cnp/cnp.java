
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class cnp
  extends JFrame
{
    public static void main(String[] args)
    {
        cnp instance = new cnp();
        instance.run(args);
    }

    public cnp()
    {
        super("cnp");
    }

    public int run(String[] args)
    {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event)
            {
                event.getWindow().setVisible(false);
                event.getWindow().dispose();
                System.exit(2);
            }
        });

        setLayout(new BorderLayout());

        this.canvas = new Canvas();
        this.getContentPane().add(this.canvas, BorderLayout.CENTER);

        pack();
        setVisible(true);

        repaint();

        return 0;
    }

    protected Canvas canvas = null;
}
