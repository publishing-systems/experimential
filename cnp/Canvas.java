
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.LineMetrics;

class Canvas extends JPanel
{
    public Canvas()
    {
        setPreferredSize(new Dimension(400, 300));
        setBackground(Color.white);
    }

    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        String text = new String("hello, world");
        int heightOffsetToBaseline = 0;

        {
            Graphics2D g2d = (Graphics2D)g.create();
            Font defaultFont = g.getFont();

            LineMetrics lineMetrics = defaultFont.getLineMetrics(text, g2d.getFontRenderContext());
            heightOffsetToBaseline = (int)lineMetrics.getAscent();

            g2d.dispose();
        }

        g.drawString(text, 1, 1 + heightOffsetToBaseline);
    }
}
